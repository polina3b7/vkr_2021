<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
})->name('index');

Route::get('/new', function () {
    return view('new');
})->name('new');

Route::get('/welcome', function () {
    return view('welcome');
})->name('welcome');

Route::get('/about', function () {
    return view('about');
})->name('about');

Route::get('/galls', function () {
    return view('galls');
})->name('galls');

Route::get('/gall-inner', function () {
    return view('gall-inner');
})->name('gall-inner');

Route::get('/orderservice', function () {
    return view('orders');
})->name('orders');

Route::get('/service-inner', function () {
    return view('service-inner');
})->name('service-inner');

Route::resource('order', OrderController::class);

Route::get('/services', 'ServiceController@index')->name('services');
Route::get('/services/{id}', 'ServiceController@show')->name('services/{id}');

Route::middleware(['role:admin'])->prefix('admin_panel')->group (function () {
      Route::get('/', [App\Http\Controllers\Admin\HomeController::class, 'index'])->name('homeAdmin');
      Route::resource('documents', \Admin\DocumentsController::class);
      Route::resource('service', \Admin\ServiceController::class);
      Route::resource('project', \Admin\ProjectController::class);
      Route::resource('user', \Admin\UserController::class);    
      Route::resource('order', OrderController::class); 

});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::middleware(['role:writer'])->resource('cabinet', CabinetController::class);      
