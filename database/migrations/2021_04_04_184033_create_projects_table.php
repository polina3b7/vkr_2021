<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('ProjectName');
            $table->date('ProjectBegin');
            $table->date('ProjectApprove');
            $table->date('ProjectDeadline');
            $table->string('ProjectStatus');
            $table->string('ProjectManager');
            $table->string('ProjectUrl');
            $table->string('ProjectImage');
            $table->foreignId('UserID')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projects');
    }
}
