<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Domains\Contracts\DocumentsContract;

class CreateDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(DocumentsContract::TABLE, function (Blueprint $table) {
            $table->id();
            $table->string(DocumentsContract::DOC_NAME);
            $table->string(DocumentsContract::DOC_URL);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(DocumentsContract::TABLE);
    }
}
