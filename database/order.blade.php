@extends ('blocks.app')
@section ('title-block')О компании@endsection
@section ('content')

<section id="order" class="order">
    <div class="container">
          <div class="block__order">
            <div class="row">
              <div class="offset-2"></div>
              <div class="col-md-10">
                <img src="img/ffon.png" class="bg__order">
                <p class="p__order">
                   Веб-студия 2Аpp была оLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo.
                </p>
              </div>
            </div>
          </div>
      </div>
</section>

<section class="brif__order">
  <div class="container">
    @if($errors->any())
                <div class="alert alert-danger">
                  <ul>
                    @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                  </ul>
                </div>
    @endif
    <div class="row">
        <form action="{{route ('costestimate-form') }}" method="post">
          @csrf
      <div class="col-12 col-md-12">
          <p> Введите Ваше имя и название компании </p>
          <input type="text" name="name" id="name" cols="180" class="brif__input2" >
          </input>
      </div>
      <div class="col-12 col-md-12">
          <p> Введите  Ваш e-mail </p>
          <input type="text" name="email" id="email" cols="180" class="brif__input2" >
          </input>
      </div>
      <div class="col-12 col-md-12">
          <p>
              {{-- KeyValuePair::where('key', 'email')->first()  --}}
              Введите Ваш номер телефона</p>
          <input type="number" name="tel" id="tel" cols="180" class="brif__input2" >
          </input>
      </div>
      <div class="col-12 col-md-12">
          <p> 1. Опишите цель создания проекта </p>
          <textarea type="text" name="goal" id="goal" cols="180" class="brif__input" >
          </textarea>
      </div>
      <div class="col-12 col-md-12">
          <p> 2. Опишите основные разделы сайта и их структуру </p>
          <textarea type="text" name="structure" id="structure" cols="180" class="brif__input" >
          </textarea>
      </div>
      <div class="col-12 col-md-12">
          <p> 3. Какие задачи должен выполнять сайт? </p>
          <textarea type="text" name="tasks" id="tasks" cols="180" class="brif__input" >
          </textarea>
      </div>
      <div class="col-12 col-md-12">
          <p> 4. Необходимо ли проведение интеграции сайта со сторонними сервисами? </p>
          <textarea type="text" name="integration" id="integration" cols="180" class="brif__input" >
          </textarea>
      </div>
      <div class="col-12 col-md-12">
          <p> 5. Каким бюджетом для проекта вы располагаете? </p>
          <textarea type="text" name="budget" id="budget" cols="180" class="brif__input" >
          </textarea>
      </div>
      <button type="submit" class="button-order">Рассчитать</button>
      </form>
    </div>
  </div>
</section>
</body>
</html>
@endsection
