<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Documents;
use Illuminate\Http\Request;
use Storage;

class DocumentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $documents = Documents::orderBy('created_at', 'desc')->get();
        return view('admin.documents.index', [
        'documents' => $documents
      ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.documents.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $new_document = new Documents ();
        $new_document->DocName = $request->DocName;
        $new_document->DocUrl = $request->DocUrl; //Storage::put('public', $request->DocUrl);
        $new_document->save();

        return redirect()->back()->withSuccess('Документ был успешно добавлен');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Documents  $documents
     * @return \Illuminate\Http\Response
     */
    public function show(Documents $documents)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Documents  $documents
     * @return \Illuminate\Http\Response
     */
    public function edit(Documents $documents)
    {
        return view('admin.documents.edit', [
          'documents'=>$documents
    ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Documents  $documents
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Documents $documents)
    {
      $documents->DocName = $request->DocName;
      $documents->DocUrl = $request->DocUrl;
      $documents->save();

      return redirect()->back()->withSuccess('Документ был успешно обновлен');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Documents  $documents
     * @return \Illuminate\Http\Response
     */
    public function destroy($documents)
    {
        Documents::find($documents)->delete();
        
        return redirect()->back()->withSuccess('Документ был успешно удален');
    }
}
