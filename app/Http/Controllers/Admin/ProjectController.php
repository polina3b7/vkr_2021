<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Project;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $project = Project::orderBy('created_at', 'desc')->get();
        return view('admin.project.index', [
            'project' => $project
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users = User::orderBy('created_at', 'desc')->get();
        return view('admin.project.create', [
            'users' => $users
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $new_project = new Project();
        $new_project->ProjectName = $request->ProjectName;
        $new_project->ProjectBegin = $request->ProjectBegin;
        $new_project->ProjectApprove = $request->ProjectApprove;
        $new_project->ProjectDeadline = $request->ProjectDeadline;
        $new_project->ProjectStatus = $request->ProjectStatus;
        $new_project->ProjectManager = $request->ProjectManager;
        $new_project->ProjectUrl = $request->ProjectUrl;
        $link = Storage::put($request->UserID, $request->ProjectImage);
        $new_project->ProjectImage = $link;
        $new_project->UserID = $request->UserID;
        $new_project->save();

        return redirect()->back()->withSuccess('Проект был успешно добавлен');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function show(Project $project)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function edit(Project $project)
    {
        $user = User::orderBy('created_at', 'desc')->get();
        return view('admin.project.edit', [
            'user' => $user,
            'project' => $project,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Project $project)
    {
        $project->ProjectName = $request->ProjectName;
        $project->ProjectBegin = $request->ProjectBegin;
        $project->ProjectApprove = $request->ProjectApprove;
        $project->ProjectDeadline = $request->ProjectDeadline;
        $project->ProjectStatus = $request->ProjectStatus;
        $project->ProjectManager = $request->ProjectManager;
        $project->ProjectUrl = $request->ProjectUrl; //Storage::put('public', $request->DocUrl);
        $project->UserID = $request->UserID;
        $project->save();

        return redirect()->back()->withSuccess('Проект был успешно обновлен');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function destroy($project)
    {
        Project::find($project)->delete();

        return redirect()->back()->withSuccess('Проект был успешно удален');
    }
}
