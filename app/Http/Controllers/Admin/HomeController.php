<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Service;
use App\Models\Order;
use App\Models\User;
use App\Models\Project;

class HomeController extends Controller
{
    public function index ()
    {
    $service_count = Service::count();
    $order_count = Order::count();
    $user_count = User::count();
    $project_count = Project::count();

    return view('admin.home.index', [
      'service_count' => $service_count,
      'order_count' => $order_count,
      'user_count' => $user_count,
      'project_count' => $project_count
    ]);
  
    }
}
