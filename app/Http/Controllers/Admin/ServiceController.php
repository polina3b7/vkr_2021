<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Service;
use App\Models\Documents;
use Illuminate\Http\Request;

class ServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $service = Service::orderBy('created_at', 'desc')->get();
        return view('admin.service.index', [
        'service' => $service
      ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $documents = Documents::orderBy('created_at', 'desc')->get();
        return view('admin.service.create', [
        'documents' => $documents
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $new_service= new Service ();
        $new_service->ServName = $request->ServName;
        $new_service->ServDiscription = $request->ServDiscription;
        $new_service->ServCost = $request->ServCost;
        $new_service->ServTime = $request->ServTime;
        $new_service->DocId = $request->DocId; //Storage::put('public', $request->DocUrl);
        $new_service->save();

        return redirect()->back()->withSuccess('Услуга была успешно добавлена');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function show(Service $service)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function edit(Service $service)
    {
        $documents = Documents::orderBy('created_at', 'desc')->get();
        return view('admin.service.edit', [
        'documents' => $documents,
        'service' => $service,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Service $service)
    {
        $service->ServName = $request->ServName;
        $service->ServDiscription = $request->ServDiscription;
        $service->ServCost = $request->ServCost;
        $service->ServTime = $request->ServTime;
        $service->DocId = $request->DocId; //Storage::put('public', $request->DocUrl);
        $service->save();

        return redirect()->back()->withSuccess('Услуга была успешно обновлена');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function destroy($service)
    {
        Service::find($service)->delete();
        
        return redirect()->back()->withSuccess('Услуга была успешно удалена');
    }
}
