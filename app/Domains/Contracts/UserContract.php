<?php
namespace App\Domains\Contracts;

use App\Domains\Contracts\MainContract;

class UserContract implements MainContract {
    const TABLE =   'users';
    const NAME =   'name';
    const EMAIL =   'email';
    const PASSWORD =   'password';
    const EMAIL_VERIFIED_AT =   'email_verified_at';

    const FILLABLE  =   [    
        self::NAME,
        self::EMAIL,
        self::PASSWORD,
        self::EMAIL_VERIFIED_AT
    ];
}