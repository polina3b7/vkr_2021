<?php
namespace App\Domains\Contracts;

class DocumentsContract implements MainContract {
    const TABLE =   'documents';
    const ID    =   'id';
    const DOC_NAME   =   'DocName';
    const DOC_URL   =   'DocUrl';

    const FILLABLE  =   [
        self::DOC_NAME,
        self::DOC_URL
    ];
}