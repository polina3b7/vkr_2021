@extends ('blocks.app')
@section ('title-block')Главная страница@endsection
@section ('content')

<section id="aboutcompany" class="aboutcompany">
    <div class="container">
          <div class="block__aboutcompany">
            <div class="row">
              <div class="offset-2"></div>
              <div class="col-12 col-md-10">
                <img src="img/ffon.png" class="bg__aboutcompany">
                <p class="p__aboutcompany">
                   Веб-студия 2Аpp была оLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo.
                </p>
              </div>
            </div>
          </div>
      </div>
</section>

<section id="photo" class="photo">
    <div class="container">
      <div class="row">
        <div class="col-12 col-md-4">
          <img src="img/company1.png" class="bg">
        </div>
        <div class="col-12  col-md-4">
          <img src="img/company3.png" class="bg">
        </div>
        <div class="col-12 col-md-4">
          <img src="img/company1.png" class="bg" >
        </div>
      </div>
    </div>
</section>


<section id="aboutcompany2" class="aboutcompany2">
    <div class="container">
          <div class="block__aboutcompany2 d-flex">
            <div class="row">
              <div class="col-12 col-md-11">
                <img src="img/ffon.png" class="bg__aboutcompany2">
                <p class="p__aboutcompany2">
                   Веб-студия 2Аpp была оLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo.
                </p>
              </div>
            </div>
      </div>
    </div>
</section>

<section id="clients" class="clients">
    <div class="container">
      <div class="row">
        <div class="col-12 col-md-3">
          <img src="img/samlogo.png" class="photo__clients">
        </div>
        <div class="col-12 col-md-3">
          <img src="img/samlogo.png" class="photo__clients">
        </div>
        <div class="col-12 col-md-3">
          <img src="img/samlogo.png" class="photo__clients">
        </div>
        <div class="col-12 col-md-3">
          <img src="img/samlogo.png" class="photo__clients">
        </div>
      </div>


    </div>
</section>

<footer id="footer" class="footer">
    <div class="container">
    <p>2020. App. Все права защищены</p>
    </div>
</footer>
</body>
</html>
@endsection
