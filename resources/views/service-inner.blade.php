@if (session('success'))
            <div class="alert alert-success" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-check"></i>{{ session('success') }}</h4>
            </div>
@endif
@extends ('blocks.app')
@section ('title-block')Страница услуг@endsection
@section ('content')
<div class="{{route ('service.show', $service['id'] )}}" method="get">
@csrf
<section id="price" class="price">
    <div class="container">
          <div class="block">
            <div class="row">
              <div class="col-md-12">
                  <h1>{{$service['ServName']}}"</h1>
                  </br></br></br>
              </div>
                  <div class="col-md-12">
                  <a href="{{$service['DocUrl']}}">Здесь Вы сможете ознакомиться с договором на разработку и брифом</a>
                  </div>

            </div>
          </div>
      </div>
</section>

<section id="price" class="price">
	<div class="container">
		<div class="row">
			<div class="col-md-4">
				<h1>{{$service['ServCost']}}</h1>
			</div>
			<div class="col-md-5">
				<h1>{{$service['ServTime']}}</h1>
			</div>
			<div class="col-md-3">
				<button class="btn-btn-info button-orderservice" data-toggle="modal" data-target="#MyModal">Заказать</button>
			</div>
		</div>
	</div>
</section>

<div id="MyModal" class="modal fade">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Заказать</h4>
          <button class="close" data-dismiss="modal">х</button>
        </div>
          <div class="modal-body">
            @if($errors->any())
                        <div class="alert alert-danger">
                          <ul>
                            @foreach($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                          </ul>
                        </div>
            @endif
            <form action="{{route ('order.store')}}" method="POST">
            @csrf
              <div class="form-group">
                <label form="name" for="name">Введите имя</label>
                <input required type="text" name="name" id="name" class="form-control">
              </div>
              <div class="form-group">
                <label form="name">Введите e-mail</label>
                <input required type="email" name="email" id="email" class="form-control">
              </div>
              <div class="form-group">
                <label form="name">Введите текст сообщения</label>
                <input required type="text" name="subject" id="subject" class="form-control">
              </div>
              <button type="submit" class="btn btn-primary">Отправить</button>
            </form>
      </div>
    </div>
  </div>
</div>  

<section id="discription" class="discription">
	<div class="container">
    <div class="discription__inner">
    <h1>Описание:</h1>
    <div class="row">
      <div class="col-md-6">
        <p>{{$service['ServDiscription']}}</p>
      </div>
      <div class="col-12 col-md-6">
        <img src="/img/fon6.png" alt="" class="img2_serviceinner">
      </div>

	</div>
  </div>
  </div>
</section>
</div>
<footer id="footer" class="footer">
    <div class="container">
    <p>2020. App. Все права защищены</p>
    </div>
</footer>

  <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>

</body>
</html>
@endsection
