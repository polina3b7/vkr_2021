@extends ('blocks.app')
@section ('title-block')Главная страница@endsection
@section ('content')

<section id="cabinet" class="cabinet">
    <div class="container">
          <div class="block__cabinet">
            <div class="row">
              <div class="offset-2"></div>
              <div class="col-md-10">
                <img src="/img/ffon.png" class="bg__order">
                <p class="p__cabinet">
                {{ Auth::user()->name }} добро пожаловать! Мы рады приветствовать Вас, и желаем Вам хорошего дня! </br>
                В своем личном кабинете Вы сможете увидеть данные о Вашем проекте. 
                </p>
              </div>
            </div>
          </div>
      </div>

</section>

<section id="cabinet_inner" class="cabinet_inner">
  <div class="container">
  @foreach (request()->user()->projects as $project)
                <h2>
                {{ $project['ProjectName']}}
                </h2>
                @endforeach
  </div>
</section>

<section id="cabinet_inner" class="cabinet_inner">
  <div class="container">
  </br>
    <div class="discription__cabinet">
    
    <div class="row">
      <div class="col-md-12">
      @foreach (request()->user()->projects as $project)
        <p class="p__discription__cabinet">
        Дата подписания договора: {{ $project['ProjectBegin']}}
        </p>
        <p class="p__discription__cabinet">
        Дата утверждения дизайна: {{ $project['ProjectApprove']}}
        </p>
        <p class="p__discription__cabinet">
        Дедлайн: {{ $project['ProjectDeadline']}}
        </p>
        <p class="p__discription__cabinet">
        Статус проекта: {{ $project['ProjectStatus']}}
        </p>
        <p class="p__discription__cabinet">
        Менеджер проекта: {{ $project['ProjectManager']}}
        </p>
        <p class="p__discription__cabinet">
        Ссылка на документы проекта: {{ $project['ProjectUrl']}}
        </p>
        @endforeach
      </div>
  </div>
  </div>
  </div>
</section>

<section id="cabinet_inner" class="cabinet_inner">
  <div class="container">
    <h1>Прикрепить документы</h1>
      <div class="row d-flex">
        <div class="col-12 col-md-3">
          <a href="#" class="button">Прикрепить логотип</a>
        </div>
        <div class="col-12 col-md-3">     
          <a href="#" class="button">Прикрепить бриф</a>
        </div>
        <div class="col-12 col-md-3">  
          <a href="#" class="button">Прикрепить утверждение</a>
       </div>
       <div class="col-12 col-md-3">  
          <a href="#" class="button">Прикрепить</a>
       </div>
       <form id="logout-form" action="{{ route('logout') }}" method="POST">
                @csrf
                <button class="btn" style="width: 50px;">
                <img style="width: 100%;" src="https://www.pngkey.com/png/full/271-2715393_logout-comments-icon.png" alt="notebook">
                </button>
  </form>
      </div>
  </div>
</section>

</body>
</html>

@endsection