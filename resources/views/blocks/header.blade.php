@section('header')
<body>
	<header id="header" class="header">
		<nav class="navbar navbar-expand-lg navbar-dark">
  <div class="container">
    <img src="/img/logo1.png" alt="Студия" class="logo">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
      <div class="navbar-nav">
        <a class="nav-link active" aria-current="page" href="{{route ('index') }}">Главная</a>
        <a class="nav-link" href="{{route ('about') }}">О нас</a>
        <a class="nav-link" href="{{route ('galls') }}">Наши работы</a>
        <a class="nav-link" href="{{route ('services') }}">Наши услуги</a>
        <a class="nav-link" href="{{route ('login') }}">Войти</a>
        <a class="nav-link" href="tel:87076365966" class="tel">87076365966</a>
      </div>
    </div>
  </div>
</nav>
</header>
