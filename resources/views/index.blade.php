@extends ('blocks.app')
@section ('title-block')Главная страница@endsection
@section ('content')
<div class="offer">
<div class="container">
	<div class="row d-flex align-items-center">
			<div class="col-12 col-md-5">
					<h1>Мы знаем, что Вы зашли к нам не просто так...</h1>
					<p>Мы создадим для Вас сайт, который станет лучшим инструментом для Ваших продаж</p>
					<a href="" class="click">Кликни здесь</a>
					<img src="img/tr.png" alt="Полезно" class="tr">
			</div>
			<div class="col-12 col-md-7">
					<img src="img/notebook1.png" alt="notebook" class="bg">
			</div>
		</div>
	</div>
</div>


	<section id="about" class="about">
		<div class="container">
						<h1>Наши преимущества</h1>
				<div class="row text-center">
                    <div class="col-12 col-md-4">
						<img src="img/clients.png" alt="Клиенты">
						<p>Более 300 довольных клиентов</p>
					</div>
				 	<div class="col-12 col-md-4">
						<img src="img/support.png" alt="Техническая поддержка" >
						<p>Бесплатная техническая поддержка</p>
					</div>
					<div class="col-12 col-md-4">
						<img src="img/garant.png" alt="Гарантия качества" >
						<p>Гарантия отличного качества</p>
					</div>
				</div>

			</div>
	</section>

		<section id="service" class="service">
		<div class="container">
				<h1>Наши услуги</h1>
				<div class="row">
				<div class="col-12 col-md-4">
					<div class="card">
						<img class="card-img" src="img/serv.png" alt="Услуга">
						<div class="card-img-overlay">
							<p>Разработка сайтов</p>
						</div>
					</div>
				</div>
				<div class="col-12 col-md-4">
						<div class="card">
							<img class="card-img" src="img/serv.png" alt="Услуга">
							<div class="card-img-overlay">
								<p>Разработка сайтов</p>
							</div>
						</div>
				</div>
				<div class="col-12 col-md-4">
						<div class="card">
							<img class="card-img" src="img/serv.png" alt="Услуга">
							<div class="card-img-overlay">
								<p>Разработка сайтов</p>
							</div>
					</div>
				</div>
		</div>
	</div>
	</section>

<section id="galls" class="galls">
	<div class="container">
		<h1>Галерея</h1>
			<div class="row d-flex">
				<div class="col-12 col-md-4">
					<img src="img/gall2.png" alt="Разработка" class="bg" ><a href="/service-inner" >
					<img src="img/gall1.png" alt="Разработка" class="bg"><a href="/service-inner" >
				</div>
				<div class=" col-12 col-md-4">
					<img src="img/gall3.png" alt="Разработка"class="bg"><a href="/service-inner" >
				</div>
				<div class="col-12 col-md-4">
					<img src="img/gall4.png" alt="Разработка" class="bg" ><a href="/service-inner" >
				</div>
			</div>
		</div>
</section>


	<footer id="footer" class="footer">
		<div class="container">
		<p>2020. App. Все права защищены</p>
		</div>
	</footer>




	<!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
</body>
</html>
@endsection
