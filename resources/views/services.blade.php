@extends ('blocks.app')
@section ('title-block')Главная страница@endsection
@section ('content')

<section id="aboutservices" class="aboutservices">
    <div class="container">
          <div class="block__aboutservices">
            <div class="row">
              <div class="offset-2"></div>
              <div class="col-md-10">
                <img src="img/ffon.png" class="bg__aboutservices">
                <p class="p__aboutservices">
                   Веб-студия 2Аpp была оLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo.
                </p>
              </div>
            </div>
          </div>
      </div>
</section>

<section id="services" class="services">
		<div class="container">

				<div class="row">
@foreach ($service as $element)
				<div class="col-md-4">
					<div class="card">
						<img class="card-img" src="img/serv2.png" alt="Услуга">
						<div class="card-img-overlay">
							<p>{{ $element['ServName']}}</p>
						</div>
					</div>
        <a href="services/{{ $element ['id']}}" class="button-orderservice">Заказать</a>
				</div>
@endforeach
      </div>

    </div>
	</section>

	<footer id="footer" class="footer">
		<div class="container">
		<p>2020. App. Все права защищены</p>
		</div>
	</footer>

  <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
</body>
</html>
@endsection
