@extends ('blocks.app')
@section ('title-block')Главная страница@endsection
@section ('content')

<section id="gallinner" class="gallinner">
    <div class="container">
          <div class="block__gallinner">
            <div class="row">
              <div class="offset-2"></div>
              <div class="bg__gallinner col-12 col-md-10 ">
                <span class="p__gallinner">
                   Веб-студия 2Аpp была оLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt
                   Веб-студия 2Аpp была оLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt
                </span>
                <a href="#" class="button__gallinner">Заказать</a>
              </div>
            </div>
          </div>
      </div>
</section>

<div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img class="d-block w-100" src="img/mak.jpg" alt="First slide">
    </div>
    <div class="carousel-item">
      <img class="d-block w-100" src="img/mak2.webp" alt="Second slide">
    </div>
    <div class="carousel-item">
      <img class="d-block w-100" src="img/mak2.webp" alt="Third slide">
    </div>
  </div>
  <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>

<section id="gallinner2" class="gallinner2">
    <div class="container">
          <div class="block__gallinner2 d-flex">
            <div class="row">
              <div class="col-12 col-md-11">
                <img src="img/ffon.png" class="bg__gallinner2">
                <p class="p__gallinner2">
                   Веб-студия 2Аpp была оLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo.
                </p>
              </div>
          </div>
      </div>
    </div>
</section>

<footer id="footer" class="footer">
    <div class="container">
    <p>2020. App. Все права защищены</p>
    </div>
</footer>

</body>
</html>
@endsection
