@extends ('layouts.admin_layout')

@section('title', 'Редактировать документ')

@section('content')
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0">Редактировать  документ: {{ $documents['DocName'] }}</h1>
      </div><!-- /.col -->
      <div class="col-sm-6">
        @if (session('success'))
            <div class="alert alert-success" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-check"></i>{{ session('success') }}</h4>
            </div>
        @endif
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.container-fluid -->
</div>
<!-- Main content -->
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
    <div class="card card-primary">
       <div class="card-header">
         <h3 class="card-title"></h3>
       </div>
       <!-- /.card-header -->
       <!-- form start -->
       <form action="{{route ('documents.update', $documents['id'] )}}" method="POST">
         @csrf
         @method('PATCH')
         <div class="card-body">
           <div class="form-group">
             <label for="exampleInputEmail1">Название документа</label>
             <input type="text" value="{{ $documents['DocName'] }}" name="DocName" class="form-control" id="exampleInputName" placeholder="Введите название документа" required >
           </div>
           <div class="form-group">
             <label for="exampleInputFile">Добавить файл</label>
             <div class="input-group">
               <div class="custom-file">
                 <input type="file" name="DocUrl" class="custom-file-input" id="exampleInputFile">
                 <label class="custom-file-label" for="exampleInputFile">Прикрепите файл</label>
               </div>
               <div class="input-group-append">
                 <span class="input-group-text">Прикрепить</span>
               </div>
             </div>
           </div>
         </div>
         <!-- /.card-body -->
         <div class="card-footer">
           <button type="submit" class="btn btn-primary">Обновить</button>
         </div>
       </form>
     </div>
  </div>
</div>
</div>
</section>
<!-- /.content -->
@endsection
