@extends ('layouts.admin_layout')

@section('title', 'Все документы')

@section('content')
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0">Все документы</h1>
      </div><!-- /.col -->
      <div class="col-sm-6">
        @if (session('success'))
            <div class="alert alert-success" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-check"></i>{{ session('success') }}</h4>
            </div>
        @endif
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.container-fluid -->
</div>
<!-- Main content -->
<section class="content">
  <div class="container-fluid">
    <!-- Default box -->
      <div class="card">
        <div class="card-header">
          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
              <i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
              <i class="fas fa-times"></i>
            </button>
          </div>
        </div>
        <div class="card-body p-0">
          <table class="table table-striped projects">
              <thead>
                  <tr>
                      <th style="width: 1%">
                          #
                      </th>
                      <th style="width: 20%">
                          Название документа
                      </th>

                  </tr>
              </thead>
              <tbody>
                @foreach ($documents as $element)
                    <td>
                        {{ $element ['id']}}
                    </td>
                    <td>
                        <a>
                            {{ $element['DocName']}}
                        </a>
                        <br/>
                        <small>
                            {{ $element ['DocUrl']}}
                        </small>
                    </td>
                    <td class="project-actions text-right">
                      <!--  <a class="btn btn-info btn-sm" href="{{route ('documents.edit', $element['id'] )}}">
                            <i class="fas fa-pencil-alt">
                            </i>
                            Редактировать
                        </a> -->
                        <form action="{{ route('documents.destroy', $element['id']) }}" method="POST"
                            style="display: inline-block">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-danger btn-sm delete-btn">
                                <i class="fas fa-trash">
                                </i>
                                Удалить
                            </button>
                        </form>
                    </td>
                </tr>
            @endforeach

              </tbody>
          </table>
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->
</div>
</section>
<!-- /.content -->
@endsection
