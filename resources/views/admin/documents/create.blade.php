@extends ('layouts.admin_layout')

@section('title', 'Добавить документ')

@section('content')
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0">Добавить документ</h1>
      </div><!-- /.col -->
      <div class="col-sm-6">
        @if (session('success'))
            <div class="alert alert-success" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-check"></i>{{ session('success') }}</h4>
            </div>
        @endif
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.container-fluid -->
</div>
<!-- Main content -->
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
    <div class="card card-primary">
       <div class="card-header">
         <h3 class="card-title"></h3>
       </div>
       <!-- /.card-header -->
       <!-- form start -->
       <form action="{{route ('documents.store')}}" method="POST">
         @csrf
         <div class="card-body">
           <div class="form-group">
             <label for="exampleInputEmail1">Название документа</label>
             <input type="text" name="DocName" class="form-control" id="exampleInputEmail1" placeholder="Введите название документа" required >
           </div>
           <div class="form-group">
             <label for="exampleInputEmail1">Ссылка на файл документа</label>
             <input type="text" name="DocUrl" class="form-control" id="exampleInputEmail1" placeholder="Загрузите ссылку на документ" required >
           </div>
           </div>
         </div>
         <!-- /.card-body -->
         <div class="card-footer">
           <button type="submit" class="btn btn-primary">Добавить</button>
         </div>
       </form>
     </div>
  </div>
</div>
</div>
</section>
<!-- /.content -->
@endsection
