@extends ('layouts.admin_layout')

@section('title', 'Редактировать проект')

@section('content')
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0">Редактировать  проект: {{ $project['ProjectName'] }}</h1>
      </div><!-- /.col -->
      <div class="col-sm-6">
        @if (session('success'))
            <div class="alert alert-success" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-check"></i>{{ session('success') }}</h4>
            </div>
        @endif
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.container-fluid -->
</div>
<!-- Main content -->
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
    <div class="card card-primary">
       <div class="card-header">
         <h3 class="card-title"></h3>
       </div>
       <!-- /.card-header -->
       <!-- form start -->
       <form action="{{route ('project.update', $project['id'] )}}" method="POST">
         @csrf
         @method('PATCH')
         <div class="card-body">
           <div class="form-group">
             <label for="exampleInputEmail1">Название проекта</label>
             <input type="text" name="ProjectName" value="{{$project['ProjectName']}}" class="form-control" id="exampleInputEmail1" placeholder="Введите название проекта" required >
           </div>
           <div class="form-group">
             <label for="exampleInputEmail1">Дата начала проекта</label>
             <input type="date" name="ProjectBegin" value="{{$project['ProjectBegin']}}" class="form-control" id="exampleInputEmail1" placeholder="Введите дату начала проекта" required >
           </div>
           <div class="form-group">
             <label for="exampleInputEmail1">Дата утверждения дизайна</label>
             <input type="date" name="ProjectApprove" value="{{$project['ProjectApprove']}}" class="form-control" id="exampleInputEmail1" placeholder="Введите дату утверждения дизайна" required >
           </div>
           <div class="form-group">
             <label for="exampleInputEmail1">Дедлайн проекта</label>
             <input type="date" name="ProjectDeadline" value="{{$project['ProjectDeadline']}}" class="form-control" id="exampleInputEmail1" placeholder="Введите дедлайн проекта" required >
           </div>
           <div class="form-group">
             <label for="exampleInputEmail1">Статус проекта</label>
             <input type="text" name="ProjectStatus" value="{{$project['ProjectStatus']}}" class="form-control" id="exampleInputEmail1" placeholder="Введите статус проекта" required >
           </div>      
           <div class="form-group">
             <label for="exampleInputEmail1">Менеджер проекта</label>
             <input type="text" name="ProjectManager" value="{{$project['ProjectManager']}}" class="form-control" id="exampleInputEmail1" placeholder="Введите имя Менеджера проекта" required >
           </div>        
           <div class="form-group">
             <label for="exampleInputEmail1">Ссылка на файлы проекта </label>
             <input type="text" name="ProjectUrl" value="{{$project['ProjectUrl']}}" class="form-control" id="exampleInputEmail1" placeholder="Загрузите ссылку с файлами на проект" required >
           </div>
           <div class="form-group">
             <label for="exampleInputEmail1">Выберите клиента</label>
           <select name='UserID' class="custom-select form-control-border border-width-2" id="exampleSelectBorderWidth2" required>
           @foreach ($user as $element)
                    <option value=" {{ $element['id'] }} " @if ( $element['id'] ) == $project['UserID']) selected @endif>{{ $element['UserID']}}</option>
          @endforeach
          </select>
         </div>
           </div>
         </div>
         <!-- /.card-body -->
         <div class="card-footer">
           <button type="submit" class="btn btn-primary">Обновить</button>
         </div>
       </form>
     </div>
  </div>
</div>
</div>
</section>
<!-- /.content -->
@endsection
