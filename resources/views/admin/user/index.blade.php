@extends('layouts.admin_layout')

<blade
    section|(%26%2339%3Btitle%26%2339%3B%2C%20%26%2339%3B%D0%92%D1%81%D0%B5%20%D0%BF%D0%BE%D0%BB%D1%8C%D0%B7%D0%BE%D0%B2%D0%B0%D1%82%D0%B5%D0%BB%D0%B8%26%2339%3B)%0D />

@section('content')
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0">Все пользователи</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                @if(session('success'))
                    <div class="alert alert-success" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h4><i class="icon fa fa-check"></i>{{ session('success') }}</h4>
                    </div>
                @endif
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <!-- Default box -->
        <div class="card">
            <div class="card-header">
                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                        <i class="fas fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                        <i class="fas fa-times"></i>
                    </button>
                </div>
            </div>
            <div class="card-body p-0">
                <table class="table table-striped projects">
                    <thead>
                        <tr>
                            <th style="width: 1%">
                                #
                            </th>
                            <th style="width: 20%">
                                Имя пользователя
                            </th>

                        </tr>
                    </thead>
                    <tbody>
                        @foreach($users as $element)
                        <tr>
                            <td>
                                {{ $element['id'] }}
                            </td>
                            <td>
                                <a>
                                    {{ $element['name'] }}
                                </a>
                                <br />
                                <small>
                                    {{ $element['email'] }}
                                </small>
                                @foreach($roles as $element)
                                    <small>
                                        {{ $element['name'] }}
                                    </small>
                                    @endforeach
                      <!--      </td>
                              <td class="project-actions text-right">
                               <a class="btn btn-info btn-sm"
                                    href="{{ route ('user.edit', $element['id'] ) }}">
                                    <i class="fas fa-pencil-alt">
                                    </i>
                                    Редактировать
                                </a>
                                <form
                                    action="{{ route('user.destroy', $element['id']) }}"
                                    method="POST" style="display: inline-block">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn btn-danger btn-sm delete-btn">
                                        <i class="fas fa-trash">
                                        </i>
                                        Удалить
                                    </button>
                                </form>
                            </td> 
                            </tr> -->
                        @endforeach
                        
                    </tbody>
                </table>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>
</section>
<!-- /.content -->
@endsection
