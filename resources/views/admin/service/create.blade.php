@extends ('layouts.admin_layout')

@section('title', 'Добавить услугу')

@section('content')
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0">Добавить услугу</h1>q
      </div><!-- /.col -->
      <div class="col-sm-6">
        @if (session('success'))
            <div class="alert alert-success" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-check"></i>{{ session('success') }}</h4>
            </div>
        @endif
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.container-fluid -->
</div>
<!-- Main content -->
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
    <div class="card card-primary">
       <div class="card-header">
         <h3 class="card-title"></h3>
       </div>
       <!-- /.card-header -->
       <!-- form start -->
       <form action="{{route ('service.store')}}" method="POST">
         @csrf
         <div class="card-body">
           <div class="form-group">
             <label for="exampleInputEmail1">Название услуги</label>
             <input type="text" name="ServName" class="form-control" id="exampleInputEmail1" placeholder="Введите название услуги" required >
           </div>
           <div class="form-group">
           <label for="exampleInputEmail1">Описание услуги</label>
              <textarea name="ServDiscription" class="editor" ></textarea>
           </div>
           <div class="form-group">
             <label for="exampleInputEmail1">Стоимость услуги</label>
             <input type="text" name="ServCost" class="form-control" id="exampleInputEmail1" placeholder="Введите стоимость услуги" required >
           </div>
           <div class="form-group">
             <label for="exampleInputEmail1">Сроки оказания услуги</label>
             <input type="text" name="ServTime" class="form-control" id="exampleInputEmail1" placeholder="Введите сроки оказания услуги" required >
           </div>
           <div class="form-group">
             <label for="exampleInputEmail1">Выберите документ</label>
           <select name='DocId' class="custom-select form-control-border border-width-2" id="exampleSelectBorderWidth2" required>
           @foreach ($documents as $element)
                    <option value="{{ $element['id']}}">{{ $element['DocUrl']}}</option>
          @endforeach
          </select>
         </div>
         </div>
         <!-- /.card-body -->
         <div class="card-footer">
           <button type="submit" class="btn btn-primary">Добавить</button>
         </div>
       </form>
     </div>
  </div>
</div>
</div>
</section>
<!-- /.content -->
@endsection
